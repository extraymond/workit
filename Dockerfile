FROM clickable/ubuntu-sdk:16.04-arm64

RUN apt update && \
  apt upgrade -y

RUN rustup default stable && \
  rustup target add aarch64-unknown-linux-gnu && \
  ln -s $(which qt5-qmake-arm-linux-gnueabihf) /usr/local/bin/qmake
