// /*
//  * Copyright (C) 2020  Raymond Yeh
//  *
//  * This program is free software: you can redistribute it and/or modify
//  * it under the terms of the GNU General Public License as published by
//  * the Free Software Foundation; version 3.
//  *
//  * workit is distributed in the hope that it will be useful,
//  * but WITHOUT ANY WARRANTY; without even the implied warranty of
//  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  * GNU General Public License for more details.
//  *
//  * You should have received a copy of the GNU General Public License
//  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
//  */
//
import QtQuick 2.7
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Ubuntu.Components.Pickers 1.3
import WorkTimer 1.0

ApplicationWindow {
  id: root
  objectName: 'mainView'

  width: units.gu(45)
  minimumWidth: units.gu(45)
  height: units.gu(75)
  minimumHeight: units.gu(75)
  visible: true

  Page {
    anchors.fill: parent

    header: PageHeader {
      id: header
      title: i18n.tr('workit')
    }

    ColumnLayout {
      spacing: units.gu(2)
      anchors {
        margins: units.gu(2)
        top: header.bottom
        left: parent.left
        right: parent.right
        bottom: parent.bottom
      }

      Item {
        Layout.fillHeight: true
      }

      Label {
        Layout.fillWidth: true
        id: label
        text: i18n.tr('Press the button below!')
      }

      Component {
        id: listDelegate
        Item {
          height: units.gu(4)
          width: parent.width

          WorkTimer {
            id: timer
          }

          RowLayout {
            anchors.fill: parent
            spacing: units.gu(2)

            Item {
              Layout.fillWidth: true
            }

            Label {
              Layout.fillWidth: true
              Layout.minimumWidth: units.gu(4)
              Layout.preferredWidth: units.gu(4)
              Layout.maximumWidth: units.gu(6)
              text: i18n.tr(modelData[1])
            }

            SpinBox {
              Layout.fillWidth: true
              Layout.maximumWidth: units.gu(12)
              id: spinbox
              value: modelData[0]
              Component.onCompleted: {
                timer.interval = spinbox.value;
              }
              onValueModified: {
                timer.interval = spinbox.value;
              }
            }

            Button {
              Layout.fillWidth: true
              Layout.minimumWidth: units.gu(2)
              Layout.maximumWidth: units.gu(10)
              Layout.preferredWidth: units.gu(10)

              text: timer.value
              onClicked: {
                timer.start_timer();
              }
            }

            Button {
              Layout.fillWidth: true
              Layout.minimumWidth: units.gu(2)
              Layout.maximumWidth: units.gu(10)
              Layout.preferredWidth: units.gu(10)

              text: i18n.tr('Cancel timer')
              enabled : timer.pending
              onClicked: {
                timer.cancel();
              }
            }

            Item {
              Layout.fillWidth: true
            }

          }

        }
      }

      ListView {
        id: listView
        model: [[1, "first"], [2, "second"], [3, "third"]]
        delegate:  listDelegate
        Layout.fillWidth: true
        Layout.preferredWidth: parent.width
        Layout.fillHeight: true
        spacing: units.gu(2)

        anchors {
          margins: units.gu(2)
          top: label.bottom
          bottom: parent.bottom
        }
      }

      Item {
        Layout.fillHeight: true
      }

    }
  }
}
