/*
 * Copyright (C) 2020  Raymond Yeh
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * workit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use cpp::*;

use qmetaobject::*;
mod plugins;
mod qrc;

fn main() {
    // appheader
    println!("hey!");

    unsafe {
        cpp! { {
            #include <QtCore/QCoreApplication>
            #include <QtCore/QString>

        }}
        cpp! {[]{
            QCoreApplication::setApplicationName(QStringLiteral("workit.extraymond"));
        }}
    }

    // set suru style
    QQuickStyle::set_style("Suru");
    // //
    // // // mount resources under qrc
    qrc::load();

    // // // register plugins to qrc
    plugins::load_plugins();

    // init engine and load the main ui
    let mut engine = QmlEngine::new();

    engine.load_file("qrc:/qml/Main.qml".into());
    engine.exec();
}
