use async_std::{prelude::*, sync, task};
use qmetaobject::*;

#[derive(QObject, Default)]
pub struct WorkTimer {
    base: qt_base_class!(trait QObject),
    interval: qt_property!(i32; NOTIFY interval_changed READ get_interval WRITE set_interval),
    interval_changed: qt_signal!(),
    status: qt_property!(bool; NOTIFY status_changed READ get_status),
    status_changed: qt_signal!(),
    value: qt_property!(QString; NOTIFY status_changed READ get_value),
    pending: qt_property!(bool; NOTIFY pending_changed READ get_pending),
    pending_changed: qt_signal!(),
    cancel: qt_method!(fn cancel(&mut self) {
        if let Some(tx) = &mut self.tx {
            task::block_on(async {
                tx.send(()).await;
            });
            self.tx = None;
            self.pending_changed();
        }
    }),
    init: qt_method!(fn init(&mut self) {
        let ptr = QPointer::from(&*self);
        let (tx, mut rx) = sync::channel::<()>(1);
        self.tx = Some(tx);
        self.pending_changed();

        let cb = queued_callback(move |()| {
            ptr.as_pinned().map(|x|
                {
                    let mut temp = x.borrow_mut();
                    temp.status = !temp.status;
                    temp.status_changed();
                });
        });
        let bg = async move {
            loop {
                task::sleep(std::time::Duration::from_secs(1)).await;
                cb(());
            }
        };

        let interval = self.interval.clone();
        let ptr = QPointer::from(&*self);
        let cb = queued_callback(move |()| {
                    ptr.as_pinned().map(|x|
                        {
                            let mut temp = x.borrow_mut();
                            temp.tx = None;
                            temp.pending_changed();
                        });
                });
        task::spawn(bg.race(async move {
            rx.next().await;
        }).race(async move {
            task::sleep(std::time::Duration::from_secs(interval as u64)).await;
            cb(());
        }));
    }),
    tx: Option<sync::Sender<()>>,
    start_timer: qt_method!(fn start_timer(&mut self) {
        self.set_status();
    }),
}

impl WorkTimer {
    fn get_pending(&self) -> bool {
        self.tx.is_some()
    }
    fn get_status(&self) -> bool {
        self.status
    }
    fn set_status(&mut self) {
        if self.tx.is_none() {
            self.init();
            self.status_changed();
        } else {
            dbg!("pending task");
        }
    }

    fn get_value(&self) -> QString {
        if self.status {
            "This is on ".into()
        } else {
            "This is off".into()
        }
    }

    fn get_interval(&self) -> i32 {
        self.interval
    }

    fn set_interval(&mut self, value: i32) {
        self.interval = value;
        self.interval_changed();
    }
}
