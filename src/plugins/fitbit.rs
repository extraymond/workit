use anyhow::Result;
use failure::Error;
use oauth2::basic::BasicClient;
use oauth2::reqwest::async_http_client;
use oauth2::{
    AsyncCodeTokenRequest, AuthType, AuthUrl, AuthorizationCode, ClientId, ClientSecret, CsrfToken,
    PkceCodeChallenge, RedirectUrl, Scope, TokenResponse, TokenUrl,
};

use async_std::fs::File;
use async_std::prelude::*;
use std::collections::HashMap;

use serde::{Deserialize, Serialize};
use std::io;

use serde_json::value::Value as jsvalue;

#[derive(Deserialize, Serialize, Debug)]
pub struct Res {
    #[serde(alias = "activities-heart")]
    activities_heart: Vec<HeartInfo>,
    #[serde(alias = "activities-heart-intraday")]
    activities_heart_intraday: HeartData,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct HeartData {
    dataset: Vec<DataPoint>,
    datasetInterval: i32,
    datasetType: String,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct DataPoint {
    time: String,
    value: i32,
}
#[derive(Deserialize, Serialize, Debug)]
pub struct HeartInfo {
    dateTime: String,
    value: Option<InfoValue>,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct InfoValue {
    customHeartRateZones: Vec<HeartRateZone>,
    heartRateZones: Vec<HeartRateZone>,
    restingHeartRate: i32,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct HeartRateZone {
    caloriesOut: f32,
    max: i32,
    min: i32,
    minutes: i32,
    name: String,
}

pub fn get_input(prompt: &str) -> String {
    println!("{}", prompt);
    let mut input = String::new();
    match io::stdin().read_line(&mut input) {
        Ok(_goes_into_input_above) => {}
        Err(_no_updates_is_fine) => {}
    }
    input.trim().to_string()
}

pub async fn build_url() -> Result<url::Url, Error> {
    let client = BasicClient::new(
        ClientId::new("22BL5T".to_string()),
        Some(ClientSecret::new(
            "5577e2008c67e9eb5b3ab45381745f07".to_string(),
        )),
        AuthUrl::new("https://www.fitbit.com/oauth2/authorize".to_string())?,
        Some(TokenUrl::new(
            "https://api.fitbit.com/oauth2/token".to_string(),
        )?),
    )
    // Set the URL the user will be redirected to after the authorization process.
    .set_redirect_url(RedirectUrl::new("http://127.0.0.1:8080".to_string())?);

    // Generate a PKCE challenge.
    let (pkce_challenge, pkce_verifier) = PkceCodeChallenge::new_random_sha256();

    // Generate the full authorization URL.
    let (auth_url, csrf_token) = client
        .authorize_url(CsrfToken::new_random)
        // Set the desired scopes.
        .use_implicit_flow()
        .add_scope(Scope::new("heartrate".into()))
        .add_scope(Scope::new("profile".into()))
        // Set the PKCE code challenge.
        // .set_pkce_challenge(pkce_challenge)
        .url();

    // This is the URL you should redirect the user to, in order to trigger the authorization
    // process.
    println!("Browse to: {}", auth_url);
    Ok(auth_url)
}

pub async fn parse_url(url: url::Url) -> Result<Option<HashMap<String, String>>> {
    if webbrowser::open(&url.into_string()).is_ok() {
        println!("we've open the window");
        let token = get_input("enter code from browser");
        println!("{}", token);

        let redir_url = url::Url::parse(&token)?;
        let frag = redir_url.fragment().unwrap();
        let kv: HashMap<String, String> = frag
            .clone()
            .split("&")
            .map(|pair| {
                let pair: Vec<String> = pair.split("=").map(|v| v.to_string()).collect();
                (pair[0].clone(), pair[1].clone())
            })
            .collect();
        Ok(Some(kv))
    } else {
        Ok(None)
    }
}

pub async fn req_data(kv: &HashMap<String, String>) -> Result<DataPoint> {
    let header = format!(
        "{} {}",
        kv.get("token_type").unwrap(),
        kv.get("access_token").unwrap()
    );
    let req = surf::get(format!(
        "https://api.fitbit.com/1/user/{}/activities/heart/date/{}/{}.json",
        kv.get("user_id").unwrap(),
        "today",
        "1d"
    ))
    .set_header("Authorization", header);

    let mut res = req.await.expect("to get response");

    let ctx = res.body_json::<Res>().await?;

    Ok(ctx
        .activities_heart_intraday
        .dataset
        .last()
        .unwrap()
        .clone())
}

pub fn get_header_file() -> Result<Header> {
    let file = std::fs::File::open("header.txt")?;
    let reader = std::io::BufReader::new(file);
    let header: Header = from_reader(reader)?;
    Ok(header)
}

pub fn write_header_to_file(header: &Header) -> Result<()> {
    let file = std::fs::File::create("header.txt")?;
    let writer = std::io::BufWriter::new(file);
    to_writer(writer, header)?;
    Ok(())
}

use serde_json::{from_reader, to_writer};

#[derive(Serialize, Deserialize)]
pub struct Header(HashMap<String, String>);

#[cfg(test)]
mod tests {
    use super::*;

    use tokio::runtime::Runtime;

    #[test]
    fn test_connect() -> Result<(), Error> {
        let mut rt = Runtime::new().unwrap();
        rt.block_on(async {
            let header: Header = {
                if get_header_file().is_err() {
                    let url = build_url().await.expect("to get url");
                    let parsed_map = parse_url(url)
                        .await
                        .expect("to parse header")
                        .expect("to get the parsed header map");
                    let header = Header(parsed_map);
                    write_header_to_file(&header).expect("able to write header file to header.txt");
                    header
                } else {
                    get_header_file().expect("to get a file")
                }
            };
            let mut datatime = String::new();
            loop {
                async_std::task::sleep(std::time::Duration::from_secs(1)).await;
                let data = req_data(&header.0).await.unwrap();
                if datatime != *&data.time {
                    datatime = data.time;
                    dbg!(data.value);
                }
            }
        });
        Ok(())
    }
}
