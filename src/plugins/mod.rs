use cstr::*;
use qmetaobject::*;
pub mod fitbit;
pub mod greeter;
pub mod work_timer;

pub fn load_plugins() {
    qml_register_type::<greeter::Greeter>(cstr!("Greeter"), 1, 0, cstr!("Greeter"));
    qml_register_type::<work_timer::WorkTimer>(cstr!("WorkTimer"), 1, 0, cstr!("WorkTimer"));
}
